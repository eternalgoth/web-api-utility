#!/bin/bash
#
# Eternal Goth Web API Config
#

# Core Configuration
language_used="en_GB.utf8"
api_url="(Your API URL)"
api_user="(Your User)"
api_key="(Your Password)"
sys_display=":0.0"

# Printer configuration
invoice_printer="(Your favourite printer)"

# File locations
cookie_file="cookies.txt"
token_file="token.txt"
jsonresults_file="results.json"

# Directories
template_dir="templates"
cache_dir="cache"
