# Eternal Goth Web API Script
Basic script to automate some tasks

## Project Description
The goal of this script is a simple goal, we need to ensure our staff can
quickly and easily manage our online orders in-store with minimal fuss.

By utilizing our own web API we're able to automatically print orders
or grab PDF versions of an order where necessary, in addition to quickly
updating the status of an order (to sent for example).

This also helps minimize web access for new or untrusted stuff, as they'll
only be able to perform actions on our store using this script, and only
what this script allows.

Thus ensuring a higher level of security and reduced risk of data leak.

## Installing & Using
Firstly, go grab the example configuration file and set the options within.

Now from then on, it's just a case of running the script with the options
you need:

```bash
./gothapi.sh help
```

That's it! The rest is self explanatory.

## License
This program is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License version 3, as published by the Free Software
Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see http://www.gnu.org/licenses/.
