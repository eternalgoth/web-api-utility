#!/bin/bash
#
#                         Eternal Goth API Script
#
# Basic script to automate tasks on our EPOS system using the website API.
#
# Version: 1.0.1
#
# Usage: ./gothapi [FLAG...] [OPTION...]
#  Options:
#    (none)                           Print this help message
#    check-orders                     Check for new online orders
#    check-tickets                    Check for new tickets and responses
#    clean                            Clean cookies, tokens and other information
#    get-pdf [order id] [saveto]      Download PDF Invoice for order
#    help                             Print this help message
#    print-order [order id]           Print invoice and packing information for an order
#    update-status [order id]         Change the status of an order (sent, collected etc)
#    manage-rewards [order id]        Manage reward points given to the customer for their order
#    sync                             Synchronize website product quantites with EPOS
#
#  Flags:
#    -c [FILE]                        Use a non-default configuration file
#
# This program is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License version 3, as published by the Free Software
# Foundation.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see http://www.gnu.org/licenses/.
#

version="1.0.1"

# Load configuration file
configFile="apiconfig.sh"

while getopts 'c:' flag; do
	case "${flag}" in
		c)
			configFile="${OPTARG}"
			hasFlag=true
		;;
	esac
done

if test -f "$configFile"; then
	source $configFile
else
	echo "ABORT! Missing config file!"
	exit 1
fi

# Setup environment
LANG=${language_used}

apiHelp() {
	echo "                        Eternal Goth API Script"
	echo
	echo "Basic script to automate tasks on our EPOS system using the website API."
	echo
	echo "Version: ${version}"
	echo
	echo "Usage: ${0} [FLAG...] [OPTION...]"
	echo " Options:"
	echo "   (none)                           Print this help message"
	echo "   check-orders                     Check for new online orders"
	echo "   check-tickets                    Check for new tickets and responses"
	echo "   clean                            Clean cookies, tokens and other information"
	echo "   get-pdf [order id] [saveto]      Download PDF Invoice for order"
	echo "   help                             Print this help message"
	echo "   print-order [order id]           Print invoice and packing information for an order"
	echo "   update-status [order id]         Change the status of an order (sent, collected etc)"
	echo "   manage-rewards [order id]        Manage reward points given to the customer for their order"
	echo "   sync                             Synchronize website product quantites with EPOS"
	echo
	echo " Flags: "
	echo "   -c [FILE]                        Use a non-default configuration file"
	echo
	echo "This program is free software; you can redistribute it and/or modify it under the"
	echo "terms of the GNU General Public License version 3, as published by the Free Software"
	echo "Foundation."
	echo
	echo "This program is distributed in the hope that it will be useful, but WITHOUT ANY"
	echo "WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR"
	echo "A PARTICULAR PURPOSE. See the GNU General Public License for more details."
	echo
	echo "You should have received a copy of the GNU General Public License along with"
	echo "this program. If not, see http://www.gnu.org/licenses/."
}

cleanFiles() {
	# Clean Session
	rm -v "${cache_dir}/${token_file}"
	rm -v "${cache_dir}/${cookie_file}"
	rm -v "${cache_dir}/${jsonresults_file}"
}

checkResult() {
	local warning="$(jq -r '."error"."warning"' ${cache_dir}/${jsonresults_file})"
	local success="$(jq -r '."success"' ${cache_dir}/${jsonresults_file})"

	if [ "$warning" != "null" ]; then
		# Oh no!
		check_result_data=${warning}
		local result=1
	elif [ "$success" != null ]; then
		# Oh? We were successful!
		check_result_data=${success}
		local result=0
	else
		# What happened? Debug me!
		local result=1
		echo "Unknown check result!"
	fi

	return $result
}

createDesktopNotification() {
	local priority=$1
	local icon=$2
	local title=$3
	local msg=$4

	DISPLAY="${sys_display}" notify-send -u "$priority" -i "$icon" "$title" "$msg"
}

apiLogin() {
	# Login to API
	if [ -f "${cache_dir}/${token_file}" ]; then
		api_token="$(< ${cache_dir}/${token_file})"

		# Validate token works
		curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/login/validateLogin&api_token=$api_token" -o "${cache_dir}/${jsonresults_file}"

		if ! checkResult; then
			echo "Current token returned warning:"
			echo "   $check_result_data"
			echo
			echo "Attempting to create new token.."
			cleanFiles
			echo

			curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" -X POST "${api_url}/login" -F username=${api_user} -F key=${api_key} | jq -r '."api_token"' > "${cache_dir}/${token_file}"
			api_token="$(< ${cache_dir}/${token_file})"

			curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/login/validateLogin&api_token=$api_token" -o "${cache_dir}/$jsonresults_file"

			if ! checkResult; then
				echo "Could not create new token! Check login details!"
			else
				echo $check_result_data
			fi
		fi
	else
		echo "Logging into API..."
		curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" -X POST "${api_url}/login" -F username=${api_user} -F key=${api_key} | jq -r '."api_token"' > "${cache_dir}/${token_file}"
		api_token="$(< ${cache_dir}/${token_file})"

		# Validate token works
		curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/login/validateLogin&api_token=$api_token" -o "${cache_dir}/${jsonresults_file}"

		if ! checkResult; then
			echo "API Server said:"
			echo "   $check_result_data"
			echo
			echo "Could not login to API! Check your login details!"

			# Clean up and exit
			rm "${cache_dir}/${token_file}"
			exit 1
		else
			echo $check_result_data
		fi
	fi
}

syncProducts() {
	apiLogin

	curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/epos/sync&api_token=$api_token" -o "${cache_dir}/${jsonresults_file}"

	if ! checkResult; then
		echo $check_result_data
	else
		echo "Successfully Synchronised Products!"
	fi
}

printOrder() {
	local order_id=$1
	echo "Printing documents for order $order_id.."

	apiLogin

	# Let's check to see if our wkhtmltopdf has been patched with QT
	# since we can only disable shrinking with the QT version
	local wkqt=$(wkhtmltopdf -V | grep qt)

	if [ "${wkqt}" ]; then
		wkhtmltopdf --disable-smart-shrinking --no-outline "${api_url}/order/packing&order_id=${order_id}&api_token=$api_token" "${cache_dir}/order-${order_id}-packing.pdf"
		wkhtmltopdf --disable-smart-shrinking --no-outline "${api_url}/order/invoice&order_id=${order_id}&api_token=$api_token" "${cache_dir}/order-${order_id}-invoice.pdf"
	else
		wkhtmltopdf "${api_url}/order/packing&order_id=${order_id}&api_token=$api_token" "${cache_dir}/order-${order_id}-packing.pdf"
		wkhtmltopdf "${api_url}/order/invoice&order_id=${order_id}&api_token=$api_token" "${cache_dir}/order-${order_id}-invoice.pdf"
	fi

	# Send document to printer
	lp -d ${invoice_printer} "${cache_dir}/order-${order_id}-packing.pdf"
	lp -d ${invoice_printer} "${cache_dir}/order-${order_id}-invoice.pdf"

	# Cleanup leftover files
	rm -v "${cache_dir}/order-${order_id}-packing.pdf"
	rm -v "${cache_dir}/order-${order_id}-invoice.pdf"
}

getOrderPDF() {
	local file_path=$2

	apiLogin

	for i in $(eval echo {$1}); do
		# Check for any left over braces..
		local order_id=$(echo $i | tr -d "{}")

		echo "Trying to get documents for order id $order_id..."

		# Let's check to see if our wkhtmltopdf has been patched with QT
		# since we can only disable shrinking with the QT version
		local wkqt=$(wkhtmltopdf -V | grep qt)

		if [ "${wkqt}" ]; then
			# We only grab the Invoice, since this function is mostly for accounting.. but uncomment below if needed..
			#wkhtmltopdf --disable-smart-shrinking --no-outline "${api_url}/order/packing&order_id=${order_id}&api_token=$api_token" "${file_path}/order-${order_id}-packing.pdf"
			wkhtmltopdf --disable-smart-shrinking --no-outline "${api_url}/order/invoice&order_id=${order_id}&api_token=$api_token" "${file_path}/order-${order_id}-invoice.pdf"
		else
			#wkhtmltopdf "${api_url}/order/packing&order_id=${order_id}&api_token=$api_token" "${file_path}/order-${order_id}-packing.pdf"
			wkhtmltopdf "${api_url}/order/invoice&order_id=${order_id}&api_token=$api_token" "${file_path}/order-${order_id}-invoice.pdf"
		fi

		if [ $? -eq 1 ]; then
			# Delete left over files if a 404 error was caused
			rm ${file_path}/order-${order_id}-invoice.pdf
		fi
	done
}

notifyOrder() {
	apiLogin

	curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/order/checkOrders&api_token=$api_token" -o "${cache_dir}/${jsonresults_file}"

	if ! checkResult; then
		echo $check_result_data
	else
		local newOrder="$(jq -c '.new_order' ${cache_dir}/${jsonresults_file})"

		if [ "$newOrder" == "true" ]; then
			local orders="$(jq -c '.orders[]' ${cache_dir}/${jsonresults_file})"

			# We have new orders!
			echo $check_result_data

			while read -r order; do
				local order_id=$(echo $order | jq -r '.order_id')

				printOrder ${order_id}
			done <<< ${orders}

			# Setup notification
			local priority="critical"
			local icon="applications-internet"
			local title="Online Order!"
			local msg="A new order has been placed online\!\n\n𝘊𝘭𝘪𝘤𝘬 𝘵𝘰 𝘥𝘪𝘴𝘮𝘪𝘴𝘴 𝘵𝘩𝘪𝘴 𝘯𝘰𝘵𝘪𝘧𝘤𝘢𝘵𝘪𝘰𝘯"

			createDesktopNotification "$priority" "$icon" "$title" "$msg"
		else
			echo $check_result_data
		fi
	fi
}

notifyTicket() {
	apiLogin

	curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" "${api_url}/ticket/checkTickets&api_token=$api_token" -o "${cache_dir}/${jsonresults_file}"

	if ! checkResult; then
		echo $check_result_data
	else
		local newMessage="$(jq -c '.new_message' ${cache_dir}/${jsonresults_file})"

		if [ "$newMessage" == "true" ]; then
			# There's a new support ticket and/or response
			echo $check_result_data

			local priority="critical"
			local icon="help-faq"
			local title="New Message!"
			local msg="A new message has arrived in our help centre and needs to be reviewed.\n\n𝘊𝘭𝘪𝘤𝘬 𝘵𝘰 𝘥𝘪𝘴𝘮𝘪𝘴𝘴 𝘵𝘩𝘪𝘴 𝘯𝘰𝘵𝘪𝘧𝘤𝘢𝘵𝘪𝘰𝘯"

			createDesktopNotification "$priority" "$icon" "$title" "$msg"
		else
			echo $check_result_data
		fi
	fi
}

addHistory() {
	local order_id=$1

	apiLogin

	cleanExit() {
		rm -f $tmpFile
		exit
	}

	getComment() {
		tmpFile=$(mktemp)
		template="${template_dir}/$1"

		echo
		echo
		echo "I'm about to take you to a fully functional text"
		echo "editor to create the comment to send to our customer."
		echo
		echo "When you've finished editing your comment"
		echo "press Control-X followed by \"Y\" and finally"
		echo "press enter to save your comment"
		echo
		echo -n "      -- Press enter to proceed to editor --"
		read

		if [ -f $template ]; then
			echo "$(cat $template)" > $tmpFile
		fi

		nano $tmpFile

		comment=$(cat $tmpFile)

		echo
		echo "You've written the following message:"
		echo
		echo -e "\033[1;31m${comment}"
		echo -e "\033[0m"
	}

	updateOrder() {
		local status_id="$1"
		local notify_user="$2"
		local override="$3"

		curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" -X POST "${api_url}/order/history&order_id=${order_id}&api_token=${api_token}" -F "order_status_id"="${status_id}" -F "notify"="${notify_user}" -F "override"="${override}" -F "comment"="${comment}" -o "${cache_dir}/${jsonresults_file}"

		if [ ! checkResult ]; then
			echo $check_result_data
		else
			echo "Order ${order_id} was successfully updated!"
		fi
	}

	markSent() {
		local status_id="3"
		local notify_user="1"
		local override="0"

		echo
		echo "OK! Preparing to mark item as Sent..."

		getComment sent

		read -r -p "Send this message to the customer? [y/N] " response

		case "$response" in
			[yY][eE][sS]|[yY])
				updateOrder ${status_id} ${notify_user} ${override}
				editRewards ${order_id} true
				cleanExit
			;;
			*)
				echo
				echo -e "\033[0;31mAborted: Order not updated!"
				echo -e "\033[0m"

				cleanExit
			;;
		esac
	}

	markCollect() {
		local status_id="23"
		local notify_user="1"
		local override="0"

		echo
		echo "OK! Preparing to mark item as Ready For Collection..."

		getComment collect

		read -r -p "Send this message to the customer? [y/N] " response

		case "$response" in
			[yY][eE][sS]|[yY])
				updateOrder ${status_id} ${notify_user} ${override}
				editRewards ${order_id} true
				cleanExit
			;;
			*)
				echo
				echo -e "\033[0;31mAborted: Order not updated!"
				echo -e "\033[0m"

				cleanExit
			;;
		esac
	}

	markCollected() {
		local status_id="24"
		local notify_user="1"
		local override="0"

		echo
		echo "OK! Preparing to mark item as Collected..."

		getComment collected

		read -r -p "Send this message to the customer? [y/N] " response

		case "$response" in
			[yY][eE][sS]|[yY])
				updateOrder ${status_id} ${notify_user} ${override}
			;;
			*)
				echo
				echo -e "\033[0;31mAborted: Order not updated!"
				echo -e "\033[0m"

				cleanExit
			;;
		esac
	}

	invalidOption() {
		echo "Invalid option!"
	}

	echo "You are modifying order ${order_id}"
	echo
	echo "What would you like to do with this order?"

	until [ "$selection" = "0" ]; do
		echo
		echo "   1) Mark as Sent"
		echo "   2) Mark as Ready For Collection"
		echo "   3) Mark as Collected"
		echo "   0) Exit"
		echo

		echo -n "Enter selection: "

		read selection

		case $selection in
			1 ) markSent ;;
			2 ) markCollect ;;
			3 ) markCollected ;;
			0 ) exit ;;
			* ) invalidOption ;;
		esac
	done
}

editRewards() {
	local order_id=$1

	apiLogin

	addPoints() {
		getPoints

		if [ ! $reward -eq 0 ]; then
			echo
			read -r -p "This order has ${reward} reward points available. Grant them now? [y/N] " response

			case "$response" in
				[yY][eE][sS]|[yY])
					curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" -X GET "${api_url}/order/addReward&order_id=${order_id}&api_token=${api_token}" -o "${cache_dir}/${jsonresults_file}"
					echo "The customer was rewarded with ${reward} points!"
				;;
				*)
					echo
					echo -e "\033[0;31mNo reward points have been issued!"
					echo -e "\033[0m"
				;;
			esac

			if [ ! ${1} == true ]; then
				exit
			fi
		else
			if [ ! ${1} == true ]; then
				echo
				echo -e "\033[0;31mWarning: There are no reward points available for this order!"
				exit
			fi
		fi
	}

	getPoints() {
		curl -s -b "${cache_dir}/${cookie_file}" -c "${cache_dir}/${cookie_file}" -X GET "${api_url}/order/getReward&order_id=${order_id}&api_token=${api_token}" -o "${cache_dir}/${jsonresults_file}"

		if [ ! checkResult ]; then
			echo $check_result_data
		else
			reward="$(jq -r '."reward"' ${cache_dir}/${jsonresults_file})"
		fi
	}

	showPoints() {
		getPoints

		echo
		echo "The customer has ${reward} points available for this order."
		exit
	}

	if [ ${2} == true ]; then
		addPoints true
	else
		echo "You are managing the reward points for order ${order_id}"
		echo
		echo "What would you like to do?"

		until [ "$selection" = "0" ]; do
			echo
			echo "   1) Show Available Reward Points"
			echo "   2) Issue Reward Points"
			echo "   0) Exit"
			echo

			echo -n "Enter selection: "

			read selection

			case $selection in
				1 ) showPoints ;;
				2 ) addPoints false ;;
				0 ) exit ;;
				* ) invalidOption ;;
			esac
		done
	fi
}


if [ $hasFlag ]; then
	# If someone uses a flag, then the bash arguments are pushed back
	# so we need to handle that. This may all need to be adjusted if we
	# start accepting more than one flag.
	arg1="${3}"
	arg2="${4}"
	arg3="${5}"
else
	arg1="${1}"
	arg2="${2}"
	arg3="${3}"
fi

if [[ -n $arg1 ]]; then
	if [ $arg1 = "check-orders" ]; then
		notifyOrder
	elif [ $arg1 = "check-tickets" ]; then
		notifyTicket
	elif [ $arg1 = "clean" ]; then
		echo "Cleaning up files..."
		cleanFiles
		exit
	elif [ $arg1 = "get-pdf" ]; then
		if [ $arg3 ]; then
			getOrderPDF $arg2 $arg3
		else
			echo "Missing Argument: You must specify both the order id and output path!"
			exit 1
		fi
	elif [ $arg1 = "help" ]; then
		apiHelp
		exit
	elif [ $arg1 = "print-order" ]; then
		if [ $arg2 ]; then
			printOrder $arg2
		else
			echo "Missing Argument: Missing Order ID!"
			exit 1
		fi
	elif [ $arg1 = "update-status" ]; then
		if [ $arg2 ]; then
			addHistory $arg2
		else
			echo "Missing Argument: Missing Order ID!"
			exit 1
		fi
	elif [ $arg1 = "manage-rewards" ]; then
		if [ $arg2 ]; then
			editRewards $arg2 false
		else
			echo "Missing Argument: Missing Order ID!"
			exit 1
		fi
	elif [ $arg1 = "sync" ]; then
		syncProducts
	else
		apiHelp
		exit
	fi
else
	apiHelp
fi
